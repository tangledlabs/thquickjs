from thquickjs.thquickjs import QuickJS


# create QucikJS object
qjs = QuickJS()

# set variable and value to context
qjs.set('x', 8).unwrap()

# get value by given variable name
v = qjs.get('x').unwrap() # v is 8

# change value of variable x in context
qjs.set('x', 12).unwrap()

# get value by given variable name
v = qjs.get('x').unwrap() # v is 12

# if unwrap doesn't rise Exception, unwrap_or will have no effects on return value
v = qjs.get('x').unwrap_or(11) # v is 12
