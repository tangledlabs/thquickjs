from thquickjs import QuickJS


# create QuckJS object
qjs = QuickJS()

# example js code
code = '''
    f = function(x) {
        return 40 + x;
    }
    
    f1 = function(x, y) {
        return x + y;
    }
'''

# evaluation of js code and unwraps the thResult and returns value
qjs.eval(code).unwrap()

# get js function and unwraps the thResult and returns value
func = qjs.get('f1').unwrap()

# call function
result = func(2, 3) # returns 5

# try to get non-existing variable from context will return None
func = qjs.get('a').unwrap() 
