from thquickjs.thquickjs import QuickJS


## handling errors
# create QucikJS object
qjs = QuickJS()

# handling error with try/except
def add_ten(n: int) -> int:
    return n + 10

py_name = 'pylam'

try:
    f = qjs.add_callable(py_name, 'unparsable').unwrap()
except TypeError as e:
    f = add_ten


# handling error with unwrap_value
def add_two(n: int) -> int:
        return n + 2

py_name = 'pylam'

# in case of Err, variable f will contain add_two function
f = qjs.add_callable(py_name, 'unparsable').unwrap_value(add_two)
f(10) # returns 12