from thquickjs.thquickjs import QuickJS


# create QucikJS object
qjs = QuickJS()

py_name = 'pylam'
py_func = lambda x: x * 10

# adding Python callable in context
qjs.add_callable(py_name, py_func).unwrap()

# get function by given variable name
f = qjs.get(py_name).unwrap() # v is 8

result = f(5) # returns value of 50

# in case of Err, this function will be assigned to varibale
def add_two(n: int) -> int:
    return n + 2

f_name = 'add_two'
py_f = lambda x: x + 2

# adding Python callable in context
qjs.add_callable(f_name, py_f).unwrap()

# get function by given variable name, in case Err should set f2 to add_two,
# in case Ok argument in unwrap_or is ignored
f2 = qjs.get(f_name).unwrap_or(add_two)
